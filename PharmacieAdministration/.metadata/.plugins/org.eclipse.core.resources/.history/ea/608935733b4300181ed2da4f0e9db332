package org.sid.web;
import java.util.List;
import org.sid.dao.MedicamentRepository;
import org.sid.entities.Medicament;

/*spring*/
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@CrossOrigin("*")
public class MedicamentRestService {
	@Autowired
	private MedicamentRepository medicamentRepository;
	
	
	/*RequestMapping pour y acceder via HTTP avec une certaine methode*/
	
	//GET
	@RequestMapping(value="/medicaments", method=RequestMethod.GET) 
	public List<Medicament> getMedicament(){
		return medicamentRepository.findAll();
	}
	
	
	//GET with param
	@RequestMapping(value="/medicaments/{id}", method=RequestMethod.GET)
	public Medicament getMedicament(@PathVariable Long id){
		return medicamentRepository.findById(id).orElse(null);
	}
	
	
	/*chercher medicaments :
	permet par exemple de chercher les medicaments par une lettre de début etc .. */
	@SuppressWarnings("deprecation")
	@RequestMapping(value="/chercherMedicaments", method=RequestMethod.GET)
	public Page<Medicament> chercher(
			@RequestParam(name="mc",defaultValue="")String mc,
			@RequestParam(name="page",defaultValue="0")int page, 
			@RequestParam(name="size",defaultValue="5")int size){
		return medicamentRepository.chercher("%"+mc+"%", new PageRequest(page,size));
	}	
	
	
	//POST
	@RequestMapping(value="/medicaments", method=RequestMethod.POST)
	public Medicament save(@RequestBody Medicament m){
		return medicamentRepository.save(m);
	}
	
	
	//DELETE
	@RequestMapping(value="/medicaments/{id}", method=RequestMethod.DELETE)
	public void delete(@PathVariable Long id){
		medicamentRepository.deleteById(id);
	}
	
	
	//MODIFY
	@RequestMapping(value="/medicaments/{id}", method=RequestMethod.PUT)
	public Medicament save(@PathVariable Long id, @RequestBody Medicament m){
		m.setId(id);
		return medicamentRepository.save(m);
	}	
	
}
