package org.sid;

import org.sid.dao.MedicamentRepository;
import org.sid.entities.Medicament;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication 
public class PharmacieAdministrationApplication extends SpringBootServletInitializer implements CommandLineRunner {
	@Autowired
	private MedicamentRepository medicamentRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(PharmacieAdministrationApplication.class, args);
	}
		
	@Override
	public void run(String... args) throws Exception {
	}
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		// TODO Auto-generated method stub
		return builder.sources(PharmacieAdministrationApplication.class);
	} 
}
