package org.sid.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Medicament implements Serializable{

	@Id @GeneratedValue
	private Long Id;
	private String Nom; 
	private String FamCode; 
	private String Composition; 
	private String Effet;
	private String ContreIndic;
	private Integer Prix;
	
	
	
	public Medicament() {
		super();
	}

	public Medicament(String nom, String famCode, String composition, String effet, String contreIndic, Integer prix) {
		super();
		Nom = nom;
		FamCode = famCode;
		Composition = composition;
		Effet = effet;
		ContreIndic = contreIndic;
		Prix = prix;
	}


	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getNom() {
		return Nom;
	}

	public void setNom(String nom) {
		Nom = nom;
	}

	public String getFamCode() {
		return FamCode;
	}

	public void setFamCode(String famCode) {
		FamCode = famCode;
	}

	public String getComposition() {
		return Composition;
	}

	public void setComposition(String composition) {
		Composition = composition;
	}

	public String getEffet() {
		return Effet;
	}

	public void setEffet(String effet) {
		Effet = effet;
	}

	public String getContreIndic() {
		return ContreIndic;
	}

	public void setContreIndic(String contreIndic) {
		ContreIndic = contreIndic;
	}

	public Integer getPrix() {
		return Prix;
	}

	public void setPrix(Integer prix) {
		Prix = prix;
	}
}
