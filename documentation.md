﻿#   Gestion des médicaments   :pill:

Ce projet à était construit dans le cadre de la mission **GSB** des PPE dans le but de permettre la gestion des médicaments présents dans la base de données. 

## Plan du réseau
 Le réseaux est composé de 4 machines de développement, d'un serveur **Apache Tomcat** qui héberge l'API, d'un serveur **Nginx** pour l'application Angular, d'un **Active directory**, d'un serveur **MySQL** et **GLPI**.

![enter image description here](https://lh3.googleusercontent.com/d-R0satQ75zbY8xCmcwEJxzKAEG9rztAxmK5uWf6E1eTE1puFxwy2tCG6pFikNJL7rm7r2yNMcJb)
## Schéma de la BDD
![enter image description here](https://lh3.googleusercontent.com/_s3mTmRRrx4eXf6846MnpXxcqLARU5ySlGa-WsNPcEsf7V7EFV_djXFNYCyxogEw14NuD6CD4i6d)

# 1 - Gestion des médicaments API

:books: **Principes d'une API Rest :** 

![enter image description here](https://lh3.googleusercontent.com/dZjnP4554nPdjp49a_cH6D0O79TB7O5EFMYNPKzxPBHjaTvD3_kSumWzTw0L4KGV5DC2jY999uo)
	
 :point_right: Une API compatible REST, ou « RESTful », est une interface de programmation d'application qui fait appel à des requêtes HTTP pour obtenir (GET), modifier(PUT), publier (POST) et supprimer (DELETE) des données.
Une API RESTful fragmente une transaction en plusieurs petits modules, chacun traitant une partie sous-jacente spécifique de la transaction. 

## Architecture de l'application
  L' API à était développée en **Java JEE** avec le framework  [**Spring**.](https://spring.io/)

**Décomposition du projet:** :file_folder:
Le projet est décomposé en en plusieurs classes réparties dans plusieurs **«packages»** en fonction de leurs rôles.

L’ API à était développée en Java JEE avec le framework  Spring.![enter image description here](https://lh3.googleusercontent.com/3ct-9WMD1dinL8sOVsWCaPLbhTgsitejZkg9e9_jqT0W9cDvHrNr_s6YqOCrwHMRsc9giamPeShC)
-   **org.sid** est constitué des classes: _**PharmacieAdministrationApplication**_ qui est le point départ de l’application, et _**SwaggerConfig**_ qui contient la configuration de la documentation automatique de l’outil swagger.
    
-   **org.sid.** **dao** est constitué de la classe: _**MedicamentRepository**_ qui est une interface Spring fournissant les méthodes de gestion des données comme **save** ou **deleteById.**
    
-   **org.sid.** **entities**  est constitué de la classe :  _**Medicament**_ qui contient toutes les méthodes (id, nom, composition …)
    
-   **org.sid.** **web**  est constitué de la classe : _**MedicamentRestService**_ qui contient toutes les méthodes de RequestMapping (getMedicament, chercher, save ..) accessible via requête HTTP .
    
-   Le package de ressource composé de la configuration Spring pour l’accès à la base de données.

## Déploiement de l'application   :rocket:
 L’API pour être utilisée par des clients(web, mobile...) et doit donc être disponible sur un serveur web.

Pour déployer l’application je l’ai compilé pour en faire un fichier **.WAR** que j’ai hébergé sur un serveur Apache Tomcat à l’aide de l’interface fournit. -> *[tutoriel](https://www.mkyong.com/spring-boot/spring-boot-deploy-war-file-to-tomcat/)*

**Commande à effectuer dans le répertoire de l'application:**

    mvn clean install

# 2 - Gestion des médicaments interface
L'application à était développée en utilisant le langage **TypeScript** et le framework [Angular](https://angular.io/) dans sa version 4.

:books: **Principes d'une application Angular(2+):**

   :point_right: Le projet est découpé en plusieurs dossier appelés «**Component**», chaque composant contient 1 fichier html, 1 fichier css, 1 fichier de spécification auto généré, et une classe au format **TypeScript**.
   
Dans Angular, les components sont partout: l'application est un Component qui affiche des components contenant des components etc…
Chaque classe utilise un « **décorateur** » qui définit son rôle (ex : la classe _app.component.ts_ utilise le décorateur @Component)

  

Le composant de base s’appelle « **app** », il englobe tout les autres composant et contient une classe TypeScript spécifique appelée « **module** » qui utilise le décorateur @NgModule et dont le rôle et la déclaration des composants, des importations et des services utilisé dans l’application. Pour plus d'informations sur la nouvelle version d'Angular *[documentation officiel](https://angular.io/docs/)*
## Architecture de l'application
**Décomposition du projet:** :file_folder:

 Extrait de l'arborescence de l'application:
![enter image description here](https://lh3.googleusercontent.com/vdlbtlRh_wCGcyrQVjHp4nZ68lybEnFi-as0x-kFEaeLaN70-bP--WKsDX-kxiZeRbkm3is2GRAU)

Schéma des composants:
![enter image description here](https://lh3.googleusercontent.com/GvS3-qQGzmpLXLkh07rZA1OL0_WnzSpdiU3QUC_uP0vTB6RK5HzpWB57Y_mNlJ3CjdOg7HHcxw7b)

## Déploiement de l'application   :rocket:

Pour déployer l’application je l’ai compilé, un dossier **dist**  est alors créer contenant l'application .
![enter image description here](https://lh3.googleusercontent.com/j6U5G3T2rC03BNqvnihaEyUnIkmIV717on2FCYeQi_dAdvGYqjMLmSXqsb8dl3ng-rGpC_xwpYYX)
Il ne reste plus qu'a l'héberger sur un serveur web en déplaçant tout le contenu du **dist** dans le répertoire **/var/http/www** du serveur à l'aide d'un outil comme [FileZilla](https://filezilla-project.org/) par exemple *(dans le cas d'un serveur Nginx)*.


**Commande à effectuer dans le répertoire de l'application:**

    ng build --prod
    

