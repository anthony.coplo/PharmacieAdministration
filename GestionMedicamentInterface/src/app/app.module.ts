/*Un module est une classse qui utilise le décorateur NgModule */

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { MedicamentComponent } from './medicament/medicament.component';
import { AboutComponent } from './about/about.component';
import { HttpModule } from '@angular/http';
import { MedicamentService } from '../services/medicament.service';
import { NewMedicamentComponent } from './new-medicament/new-medicament.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EditMedicamentComponent } from './edit-medicament/edit-medicament.component';


/* URL rounting */
const appRoutes:Routes=[
  { path: 'about',component:AboutComponent },
  { path: 'medicament', component: MedicamentComponent },
  { path: 'new-medicament', component: NewMedicamentComponent },
  { path: 'editMedicament/:id', component: EditMedicamentComponent },
  { path: '', redirectTo: '/about',pathMatch:'full' },
]

@NgModule({ //on declare ici tout ce que l'on utilise
  declarations: [
    AppComponent,
    MedicamentComponent,
    AboutComponent,
    NewMedicamentComponent,
    EditMedicamentComponent,
  ],
  imports: [ 
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpModule,
    FormsModule
  ],
  providers: [MedicamentService], //utilisation du service
  bootstrap: [AppComponent]
})
export class AppModule { }
