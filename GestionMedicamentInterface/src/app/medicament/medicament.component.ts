import { Component, OnInit } from '@angular/core';
import { Http } from "@angular/http";
import "rxjs/add/operator/map";
import { MedicamentService } from '../../services/medicament.service';
import { Router } from '@angular/router';
import { Medicament } from '../../model/model.medicament';

@Component({
  selector: 'app-m',
  templateUrl: './medicament.component.html',
  styleUrls: ['./medicament.component.css']
})

export class MedicamentComponent implements OnInit {
  pageMedicaments:any;
  currentPage:number=0;
  motCle:string="";
  pages: Array<number>;
  size:number=5;
  
  constructor(private http:Http, public medicamentService:MedicamentService, public router:Router) { }


  ngOnInit() { } //une fois que le composant est chargé tout ce qui est a l'interieur s'execute 

  /* Methode qui liste tout les medicaments a partir d'un mot cle */
  doSearch(){
    this.medicamentService.getMedicaments(this.motCle,this.currentPage,this.size).subscribe(data => {
      this.pageMedicaments = data;
      this.pages = new Array(data.totalPages);
      console.log("OK");
    }, err => {
      console.log(err);
    })
  }

  /* methode qui déclenche doSearch */
  Chercher(){
    this.doSearch();
  }

  /* methode pour la pagination */
  goToPage(i:number){
    this.currentPage=i;
    this.doSearch();
  }

  /* methode qui déclenche le changement d'URL apres action sur EDIT btn */
  onEditMedicament(id:number){
    this.router.navigate(['editMedicament',id])
  }

  /* methode appellée après action sur DELETE btn qui déclenche deleteMedicament */
  onDeleteMedicament(m: Medicament) {
    let confirm=window.confirm("Est vous sûre de vouloir supprimmer ce médicament ?")
    if(confirm==true){ // verif du choix 
      window.alert("Element supprimmé .")
      this.medicamentService.deleteMedicament(m.id)
        .subscribe(data=>{
          this.pageMedicaments.content.splice(
            this.pageMedicaments.content.indexOf(m),1
          );
        },err=>{
          console.log(err);
        })
    }
  }

}
