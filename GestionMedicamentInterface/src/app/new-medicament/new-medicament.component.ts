import { Component, OnInit } from '@angular/core';
import { Medicament } from '../../model/model.medicament';
import { MedicamentService } from '../../services/medicament.service';

@Component({
  selector: 'app-new-medicament',
  templateUrl: './new-medicament.component.html',
  styleUrls: ['./new-medicament.component.css']
})

export class NewMedicamentComponent implements OnInit {
  medicament:Medicament= new Medicament();

  constructor(public medicamentsService:MedicamentService) { }

  ngOnInit() {
  }

  saveMedicament(){
    this.medicamentsService.saveMedicament(this.medicament)
      .subscribe(data=>{ 
        console.log(data)
      }, err=>{
        console.log(err);
      })
  }
}