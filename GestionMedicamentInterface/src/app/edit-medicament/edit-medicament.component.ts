import { Component, OnInit } from '@angular/core';
import { Medicament } from '../../model/model.medicament';
import { MedicamentService } from '../../services/medicament.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-medicament',
  templateUrl: './edit-medicament.component.html',
  styleUrls: ['./edit-medicament.component.css']
})
export class EditMedicamentComponent implements OnInit {
  mode:number=1;
  medicament:Medicament=new Medicament();
  idMedicament:number;

  constructor(public activatedRoute: ActivatedRoute, public medicamentService:MedicamentService) {  //donne la route activée  //injection du service
    //console.log(activatedRoute.snapshot.params['id']);
    this.idMedicament=activatedRoute.snapshot.params['id'];
  }

  ngOnInit() {
    this.medicamentService.getMedicament(this.idMedicament)
      .subscribe(data=>{
        this.medicament=data;
      },err=>{
        console.log(err);
      })
  }


  updateMedicament(){
    this.medicamentService.updateMedicament(this.medicament)
      .subscribe(data => {
        console.log(data);
        alert("Mise à jour effectuée");
      }, err => {
        console.log(err);
        alert("Erreur lors de la MAJ"+err);
      })
  }
}