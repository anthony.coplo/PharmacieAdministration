import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Medicament } from "../model/model.medicament";

@Injectable()
export class MedicamentService{

    //constructeur
    constructor(public http:Http){}
                                    
/*
                                         ___________________________________
                                        |                                   |
                                        |           Méthodes DAO            |
                                        |___________________________________|
*/

    getMedicaments(motCle:string,page:number,size:number){
        return this.http.get("http://172.20.103.5:8080/GSB_WS/chercherMedicaments?mc="+motCle+"&size="+size+"&page="+page)
            .map(resp => resp.json()); //converti le contenu de la reponse vers le format json
    }

    getMedicament( id: number) {
        return this.http.get("http://172.20.103.5:8080/GSB_WS/medicaments/" + id )
            .map(resp => resp.json()); 
    }
    
    saveMedicament(medicament:Medicament){
        return this.http.post("http://172.20.103.5:8080/GSB_WS/medicaments",medicament)
            .map(resp => resp.json());
    }

    updateMedicament(medicament: Medicament) {
        return this.http.put("http://172.20.103.5:8080/GSB_WS/medicaments/"+ medicament.id, medicament)
            .map(resp => resp.json());
    }

    deleteMedicament(id: number) {
        return this.http.delete("http://172.20.103.5:8080/GSB_WS/medicaments/" + id )
            .map(resp => resp.json());
    }
}