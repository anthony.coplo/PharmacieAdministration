export class Medicament {
    
    id:number=0;
    composition: string="";
    contreIndic: string="";
    effet: string ="";
    famCode: string=""; 
    nom: string=""; 
    prix: number=0;

}